# mgif

## WINDOWS Version
This is a simple gif picker "script". It uses the GIPHY-API to search a bunch of gifs as fast as possible out of your console. 

[![N|Solid](https://gitlab.com/Laksuh/mgif/-/raw/master/PoweredByGIPHY.png)](POWERED_BY_GIPHY)

### Useage

open the command prompt or press WIN-Key + R and type
mgif [term you want gifs for] 

f.e.
```sh 
mgif spongebob 
```

or with spaces (as %20)
```sh 
mgif good%20morning
```

Your browser will open with a hand of gifs. If you click at one, the browser closes and the gif will be downloaded and automatically copied to your clipboard, so you can paste it. (f.e. MS Teams/Telegram/...)


[![demo](https://gitlab.com/Laksuh/mgif/-/raw/master/example.gif)](https://gitlab.com/Laksuh/mgif/-/raw/master/example.gif)
### Installation
to get mgif to work you need to clone the project into your download directory. 
f.e.
```sh
cd C:\Users\YOURUSERNAME\Downloads
git clone git@gitlab.com:Laksuh/mgif.git
```

### Preparation
Now the repo should has its files in C:\Users\YOURUSERNAME\Downloads\mgif.

To get it work out of your command prompt, you need to add mgif to your environment variables.
Therefore I recommend this link: https://docs.oracle.com/en/database/oracle/r-enterprise/1.5.1/oread/creating-and-modifying-environment-variables-on-windows.html

Press *Add* (user environment variables, not system) or *Edit* if you already can find one named *Path* and add your mgif directory path to it: C:\Users\YOURUSERNAME\Downloads\mgif

Now you need your own API-Key by Giphy, which you can get here:
https://developers.giphy.com/
after getting your own, you need to set it in the **index.html** file where *APIKEYGOESHERE* is written.

The script is simple and will work with chrome. If you are using FireFox or another Browser, simply change the name in the **mgif.bat**


